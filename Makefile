
# General configuration

CC      := mpiCC
CCFLAGS := -O2
PODFLAG := -c 'User Contributed OAR Documentation' -r 'LEGI Trokata'

TARGET_C    := mpilauncher
TARGET_MAN  := mpilauncher.1    oar-envsh.1    oar-dispatch.1    oar-parexec.1    oar-profile.1
TARGET_HTML := mpilauncher.html oar-envsh.html oar-dispatch.html oar-parexec.html oar-profile.html

PREFIX     :=/opt/oar/current
PREFIX_MAN  =$(PREFIX)/man
PREFIX_ETC  =$(PREFIX)/etc


# User specific configuration

sinclude config.mk


# Target and rules

.PHONY: all clean distclean install update sync upload

all: $(TARGET_C) $(TARGET_MAN) $(TARGET_HTML)

clean:
	rm -f *.o

distclean: clean
	rm -f mpilauncher

install: update

update:
	@install -d -m 0755 -o root -g root $(PREFIX)/bin
	@install -d -m 0755 -o root -g root $(PREFIX_MAN)/man1
	@install -d -m 0755 -o root -g root $(PREFIX_ETC)/
	install -m 0755 -o root -g root oar-parexec $(PREFIX)/bin
	install -m 0755 -o root -g root oar-dispatch $(PREFIX)/bin
	install -m 0755 -o root -g root mpilauncher $(PREFIX)/bin
	install -m 0644 -o root -g root oar-parexec.1 $(PREFIX_MAN)/man1
	install -m 0644 -o root -g root oar-dispatch.1 $(PREFIX_MAN)/man1
	install -m 0644 -o root -g root mpilauncher.1 $(PREFIX_MAN)/man1
	install -m 0644 -o root -g root oar-envsh.1 $(PREFIX_MAN)/man1
	install -m 0644 -o root -g root oar-profile.1 $(PREFIX_MAN)/man1
	install -m 0644 -o root -g root oar-profile.sh $(PREFIX_ETC)

sync:
	svn update

upload:
	cadaver --rcfile=cadaverrc

mpilauncher: mpilauncher.o
	$(CC) $(CCFLAGS) -o $@ $<

%.o: %.cpp
	$(CC) $(CCFLAGS) -c $<

%.1: %.cpp
	sed -re 's#^[/ ]\* ?##; s#^// ?##' $< | pod2man -n $* $(PODFLAG) > $@

%.html: %.cpp
	sed -re 's#^[/ ]\* ?##; s#^// ?##' $< | pod2html > $@

%.1: %.sh
	pod2man -n $* $(PODFLAG) $< > $@

%.html: %.sh
	pod2html $< > $@

%.1: %
	pod2man -n $* $(PODFLAG) $< > $@

%.html: %
	pod2html $< > $@

# astyle --style=banner -s3 mpilauncher.cpp
# perltidy -i=3 -ci=3 -icp -icb -pt=2 -bt=2 -bbt=2 -b oar-parexec
