# !OarUtils - Ensemble d'outils lié à OAR et au calcul en particulier

[http://oar.imag.fr/ OAR] est un gestionnaire de ressources et de tâches
dont la fonction première est de gérer l'ordonnancement des travaux (job)
sur un cluster.

OAR fonctionne globalement très bien, nous avons cependant détecté des points
qui peuvent s'avérer pénalisant mais dont nous avons trouvé des solutions au
final asse simple.

## Variable d'environnement

Il peut y avoir un soucis avec les jobs MPI lorsque ceux-ci s'étendent sur plus
d'une machine.
La connexion inter-machine ne fonctionnant pas directement avec ```ssh```
il faut utiliser le wrapper ```oarsh``` fournit.
Cependant, celui-ci ne transmet pas toutes les variables d'environnements...
ce qui empêche le bon fonctionnement de nombreux code.

Nous avons donc développé deux solutions :

 * un wrapper de wrapper du nom d'[http://servforge.legi.grenoble-inp.fr/pub/soft-trokata/oarutils/oar-envsh.html oar-envsh].
   Un moyen simple d'utiliser la dernière version sans récupérer tout le repository est de faire :
   ```
wget http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/oarutils/oar-envsh
chmod u+x ./oar-envsh
```
 * une fonction Bash utilisant l'option -x de ```mpirun``` de nom ```oar_envmpirun```.

## Outils

Nous avons la problématique de lancer des milliers de petits jobs séquentiels en parallèle.
OAR supporte actuellement mal la charge d'avoir 20000 jobs dans la file d'attente.

Pour se faire, nous avons développé deux outils complémentaire :

 * [http://servforge.legi.grenoble-inp.fr/pub/soft-trokata/oarutils/mpilauncher.html mpilauncher],
   un petit code MPI en C++ qui divise le nombre de travaux par le 
   nombre de coeur du job maître. Chaque job est placé dans un thread MPI
   et lancé à la queue le leu.
   A noter que ```mpilauncher``` n'a aucune dépendance directe envers OAR.

 * [http://servforge.legi.grenoble-inp.fr/pub/soft-trokata/oarutils/oar-parexec.html oar-parexec],
   un petit code en Perl utilisant [http://search.cpan.org/dist/Coro/ Coro].
   Chaque job est distribué de manière asynchrone et via ```oarsh```,
   sur un des cœurs attribués au processus maître.
   A noter qu'```oar-parexec``` distribue de manière optimale les jobs sur un cluster non homogène,
   ou si ces jobs n'ont pas un temps de calcul homogène.
   Un moyen simple d'utiliser la dernière version sans récupérer tout le repository est de faire :
   ```
wget http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/oarutils/oar-parexec
chmod u+x ./oar-parexec
```

Un troisième outil viens compléter le dispositif afin de gérer des processus pas forcément séquentiels.
Cet outil laisse  OAR faire tout le travail d'ordonnancement et de placement...

 * [http://servforge.legi.grenoble-inp.fr/pub/soft-trokata/oarutils/oar-dispatch.html oar-dispatch],
   un petit code en Perl utilisant aussi [http://search.cpan.org/dist/Coro/ Coro].
   Comme ```oar-parexec```, le code est donc asynchrone.
   Cependant, il prend en entrée une liste de job OAR
   dont il limitera le nombre dans la file d'attente d'OAR.
   Pour fonctionner, cette commande doit être lancée soit sur une frontale,
   soit dans un container OAR.
   Les sous jobs étant des job OAR à part entière (CPUSET...),
   il faut leur définir des ressources et peuvent donc être eux-mêmes parallèles...


## Repository

L'ensemble du code est sous **licence libre**.
Les scripts en ```bash``` sont sous GPL version 3 ou plus récente (http://www.gnu.org/licenses/gpl.html),
les sources ```C++``` sont sous GPL version 2 ou plus récente,
les scripts en ```perl``` sont sous la même licence que ```perl```
c'est à dire la double licence GPL et Artistic Licence (http://dev.perl.org/licenses/artistic.html).

Tous les sources sont disponibles sur la forge du LEGI :
http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/oarutils

Les sources sont gérés via subversion (http://subversion.tigris.org/).
Il est très facile de rester synchronisé par rapport à ces sources.

 * la récupération initiale
```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/oarutils
```
 * les mises à jour par la suite
```bash
git pull
```

Il est possible d'avoir un accès en écriture à la forge
sur demande motivée à [Gabriel Moreau](mailto:Gabriel.Moreau__AT__legi.grenoble-inp.fr).
Pour des questions de temps d'administration et de sécurité,
la forge n'est pas accessible en écriture sans autorisation.
Pour des questions de décentralisation du web, d'autonomie
et de non allégeance au centralisme ambiant (et nord américain),
nous utilisons notre propre forge...

Vous pouvez proposer un patch par courriel d'un fichier particulier via la commande ```diff```.
A noter que ```svn``` propose par défaut le format unifié (```-u```).
Deux exemples :
```bash
diff -u oar-parexec.org oar-parexec.new > oar-parexec.patch
svn diff oar-parexec > oar-parexec.patch
```
On applique le patch (après l'avoir lu et relu) via la commande
```bash
patch -p0 < oar-parexec.patch
```
