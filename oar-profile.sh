if [ -d /opt/oar/current/bin/ ]
then
   export PATH=${PATH}:/opt/oar/current/bin/
   export MANPATH=${MANPATH}:/opt/oar/current/man/

   # OpenMPI connector
   export OMPI_MCA_plm_rsh_agent=oarsh

   # MPI NodeFile on second interface eth1
   OAR_MSG_NODEFILE=${OAR_NODEFILE}-msg
   [ -e "${OAR_MSG_NODEFILE}" ] && export OAR_MSG_NODEFILE

   function oar_fixldpath () {
      if [ -n "${LD_LIBRARY_PATH:+x}" ]
      then
         for ldpath in $(for lib in $(/sbin/ldconfig -p | awk 'BEGIN { FS = "=> " } ; {print $2}'); do dirname ${lib}; done | sort -u)
         do
            LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ldpath}"
         done
      fi
      }
   declare -fx oar_fixldpath

   function oar_mpirun () {
      local exists_np=''
      local exists_mf=''
      local exists_vb=''

      for opt in $*
      do
         [ "${opt}" == "-np" ]           && exists_np='y'
         [ "${opt}" == "-machinefile" ]  && exists_mf='y'
         [ "${opt}" == "--machinefile" ] && exists_mf='y'
         [ "${opt}" == "-hostfile" ]     && exists_mf='y'
         [ "${opt}" == "--hostfile" ]    && exists_mf='y'
         [ "${opt}" == "--host" ]        && exists_mf='y'
         [ "${opt}" == "-host" ]         && exists_mf='y'
         [ "${opt}" == "-H" ]            && exists_mf='y'
         [ "${opt}" == "--verbose" ]     && exists_vb='y'
         [ "${opt}" == "-v" ]            && exists_vb='y'
         [ "${opt}" == "--" ]            && break
      done

      [ -n "${VERBOSE:+x}" ] && exists_vb='y'

      local mpi_option=''

      # NODEFILE to use. By default specialized one
      local nodefile="${OAR_NODE_FILE}"
      [ -n "${OAR_MSG_NODEFILE:+x}" -a -e "${OAR_MSG_NODEFILE}" ] && nodefile="${OAR_MSG_NODEFILE}"
      if [ -z ${exists_mf} ]
      then
         [ -e "${nodefile}" ] && mpi_option="-machinefile ${nodefile} ${mpi_option}"
      fi

      # Number of MPI parallel run. By default MAX.
      local nbcpus=''
      [ -e "${nodefile}" ]  && nbcpus=$(cat ${nodefile} | wc -l)
      [ -n "${OAR_NP:+x}" ] && nbcpus=${OAR_NP}
      if [ -z ${exists_np} ]
      then
         [ -z ${nbcpus} ] || mpi_option="-np ${nbcpus} ${mpi_option}"
      fi

      # VARIABLE to define in other MPI parallel run
      for varenv in $(env \
         | grep "^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]" \
         | egrep -v "^USER=|^TERM=|^OAR|^SGE_|^LS_COLORS=\
            |^ENV=|^BASH_ENV=|^HOSTNAME=|^LOGNAME=|^MAIL=\
            |^MANPATH=|^OMPI_MCA_plm_rsh_agent=|^PWD=\
            |^SHELL=|^SSH_|^SUDO_COMMAND=|^HOME=|^DISPLAY=\
            |^SHLVL=" \
         | cut -f 1 -d '=' \
         | sort )
      do
         mpi_option="${mpi_option} -x ${varenv}"
      done

      # Special case of LD_LIBRARY_PATH and system path
      local ld_path="${LD_LIBRARY_PATH}"
      oar_fixldpath

      [ ! -z ${exists_vb} ] && echo mpirun ${mpi_option} $*
      mpirun ${mpi_option} $*
      [ ! -z ${ld_path} ] && LD_LIBRARY_PATH="${ld_path}"
      }
   declare -fx oar_mpirun

   function oar_envmpirun () {
      local exists_np=''
      local exists_mf=''
      local exists_vb=''

      for opt in $*
      do
         [ "${opt}" == "-np" ]           && exists_np='y'
         [ "${opt}" == "-machinefile" ]  && exists_mf='y'
         [ "${opt}" == "--machinefile" ] && exists_mf='y'
         [ "${opt}" == "-hostfile" ]     && exists_mf='y'
         [ "${opt}" == "--hostfile" ]    && exists_mf='y'
         [ "${opt}" == "--host" ]        && exists_mf='y'
         [ "${opt}" == "-host" ]         && exists_mf='y'
         [ "${opt}" == "-H" ]            && exists_mf='y'
         [ "${opt}" == "--verbose" ]     && exists_vb='y'
         [ "${opt}" == "-v" ]            && exists_vb='y'
         [ "${opt}" == "--" ]            && break
      done

      [ -n "${VERBOSE:+x}" ] && exists_vb='y'

      local mpi_option=''

      # NODEFILE to use. By default specialized one
      local nodefile="${OAR_NODE_FILE}"
      [ -n "${OAR_MSG_NODEFILE:+x}" -a -e "${OAR_MSG_NODEFILE}" ] && nodefile="${OAR_MSG_NODEFILE}"
      if [ -z ${exists_mf} ]
      then
         [ -e "${nodefile}" ] && mpi_option="-machinefile ${nodefile} ${mpi_option}"
      fi

      # Number of MPI parallel run. By default MAX.
      local nbcpus=''
      [ -e "${nodefile}" ]  && nbcpus=$(cat ${nodefile} | wc -l)
      [ -n "${OAR_NP:+x}" ] && nbcpus=${OAR_NP}
      if [ -z ${exists_np} ]
      then
         [ -z ${nbcpus} ] || mpi_option="-np ${nbcpus} ${mpi_option}"
      fi

      # Change login remote shell
      local rsh_agent="${OMPI_MCA_plm_rsh_agent}"
      if [ "${OMPI_MCA_plm_rsh_agent}" == "oarsh" ]
      then
         OMPI_MCA_plm_rsh_agent="oar-envsh"
      fi

      [ ! -z ${exists_vb} ] && echo mpirun ${mpi_option} $*
      mpirun ${mpi_option} $*
      [ ! -z ${rsh_agent} ] && OMPI_MCA_plm_rsh_agent="${rsh_agent}"
      }
   declare -fx oar_envmpirun

   if [ -n "$PS1" -a -n "${OAR_JOB_WALLTIME_SECONDS}" ]
   then
      ( sleep $(( ${OAR_JOB_WALLTIME_SECONDS} - (10*60) )); \
         echo; echo; echo "Close interactive session in 10min"; echo; \
         sleep 0.2; printf \\a; \
         sleep 0.2; printf \\a; \
         sleep 0.2; printf \\a; \
         sleep 8m; \
         echo; echo; echo "Close interactive session in 2min"; echo; \
         sleep 0.2; printf \\a; \
         sleep 0.2; printf \\a; \
         sleep 0.2; printf \\a; \
         sleep 0.2; printf \\a; \
         sleep 0.2; printf \\a; \
         ) &
   fi

fi

return

################################################################
# Documentation in POD format (like Perl)
################################################################

=head1 NAME

oar-profile.sh - OAR bash profile to source

=head1 SYNOPSIS

 source oar-profile.sh

 oar_mpirun [mpi_run option] command
 oar_envmpirun [mpi_run option] command


=head1 DESCRIPTION

The use of MPI over a cluster need some parameters.
When you use a batch scheduler like OAR,
the most important option like C<-np> and C<-machinefile> could be defined by default for most case.

This bash sript define two function C<oar_mpirun> and C<oar_envmpirun>.
These are just wrapper and for most of the case,
defined C<-np> and C<-machinefile> for you.
With on of these wrapper, it is possible possible to use C<mpi_run>
without any option...

Because C<mpi_run> need to launch a command on a remote node on a cluster,
theses wrappers are actually only validate for B<OpenMPI>
which use C<ssh> by default as its transport from one node to another.

=head1 COMMAND

=over 12

=item B<oar_mpirun> [mpi_run option] F<program>

Wrapper to C<mpirun>, use C<oarsh> as remote shell,
set environment variable on the remote side by using option C<-x> of C<mpirun>.
If no option C<-np> or C<-machinefile> are defined,
set them by default from OAR environment variable C<OAR_NODE_FILE>.
Option C<-v> (or C<--verbose>) make an echo of the real C<mpirun> command on F<stdout>.

All env variable are export by C<mpirun> except the following

 USER TERM OAR* SGE_* LS_COLORS
 ENV BASH_ENV HOSTNAME LOGNAME MAIL
 MANPATH OMPI_MCA_plm_rsh_agent PWD
 SHELL SSH_* SUDO_COMMAND= HOME DISPLAY
 SHLVL

=item B<oar_envmpirun> [mpi_run option] F<program>

Wrapper to C<mpirun>, use C<oar-envsh> as remote shell which transmit environment variable
(use the variable C<OMPI_MCA_plm_rsh_agent> for that).
If no option C<-np> or C<-machinefile> are defined,
set them by default from OAR environment variable C<OAR_NODE_FILE>.
Option C<-v> (or C<--verbose>) make an echo of the real C<mpirun> command on F<stdout>.

C<oar-envsh> export all the environment variable except the same list as above.

=back

You can use C<--machinefile>, C<-hostfile>, C<--hostfile> as alias for C<-machinefile>.
If C<--host> C<-host> or C<-H> are defined, no option C<-machinefile> is set.

At the end of the C<mpirun> option, you can use C<-->.
Wrapper will then stop to analyse the command line.
It could be important to use that if your F<program> has the same names for command line option...

=head1 SEE ALSO

mpirun, oarsh, oar-envsh

=head1 AUTHORS

Written by Gabriel Moreau, Grenoble - France

=head1 LICENSE AND COPYRIGHT

GPL version 2 or later

Copyright (C) 2011-2013 Gabriel Moreau / LEGI - CNRS UMR 5519 - France
